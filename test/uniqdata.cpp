#include <array>
#include <assert.h>
#include <uniqdata.hpp>
#include <vector>
auto test_constructors() {
	Uniqdata<i32> d1;
	assert(d1.data() == nullptr);
	Uniqdata<i32> d2(20);
	assert(d2.size() == 20);
	for (const auto &e : d2) {
		assert(e == 0);
	}
	Uniqdata<f32> d3({1.0f, 2.0f, 3.0f});
	assert(d3[0] == 1.0f);
	assert(d3[1] == 2.0f);
	assert(d3[2] == 3.0f);
	Uniqdata<f32> d4(std::move(d3));
	assert(d3.size() == 0);
	assert(d3.data() == nullptr);
	assert(d4[0] == 1.0f);
	assert(d4[1] == 2.0f);
	assert(d4[2] == 3.0f);
	std::vector<u16> v1 = {1, 2, 3};
	Uniqdata<u16> d5(v1);
	assert(d5.size() == v1.size());
	assert(d5[0] == v1[0]);
	assert(d5[1] == v1[1]);
	assert(d5[2] == v1[2]);
	Uniqdata<u16> d6(d5);
	assert(d5.size() == d6.size());
	assert(d5[0] == d6[0]);
	assert(d5[1] == d6[1]);
	assert(d5[2] == d6[2]);
}
auto test_functions() {
	Uniqdata<u32> d1 = {1, 2, 3, 4};
	d1.resize(5);
	assert(d1.size() == 5);
	assert(d1[4] == 0);
	assert(d1[2] == 3);
	auto v1 = d1.as_vec();
	assert(v1.size() == d1.size());
	for (u32 i = 0; i < v1.size(); i++) {
		assert(v1[i] == d1[i]);
	}

	auto s1 = d1.as_span();
	assert(s1.size() == d1.size());
	for (u32 i = 0; i < s1.size(); i++) {
		assert(s1[i] == d1[i]);
	}

	auto [ptr, size] = d1.as_tuple();
	assert(ptr == d1.data());
	assert(size == d1.size());

	std::array<i32, 3> a1{1, 2, 3};
	auto s2 = std::span(a1);
	d1.push(s2);
	assert(d1.size() == 8);
	assert(d1[6] == 2);

	std::vector<f32> v2 = {1, 2, 3};
	auto s3 = std::span(v2);
	d1.push(s3);
	assert(d1.size() == 11);
	assert(d1[9] == 2);

	Uniqdata<u32> d2;
	d2 = d1;
	assert(d2.size() == d1.size());
	assert(d2[4] == d1[4]);
	assert(d2[10] == d1[10]);


}
auto main() -> int {
	test_constructors();
	test_functions();
	return 0;
}