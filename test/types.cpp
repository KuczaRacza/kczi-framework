#include <assert.h>
#include <types.hpp>
auto test_pixeldata_convert(const u32 &color) -> u32 {
  PixelData px(color);
  return px.get();
}
auto test_u24_convert(const u32 &color) -> u32 {
  u24 px;
  px = color;
  return px.as_u32();
}

auto main() -> int {

  assert(test_pixeldata_convert(0) == 0);
  assert(test_pixeldata_convert(255) == 255);
  assert(test_pixeldata_convert(0xff00ff00) == 0xff00ff00);

  assert(test_u24_convert(0) == 0);
  assert(test_u24_convert(230) == 230);
  assert(test_u24_convert(16384) == 16384);

  return 0;
}