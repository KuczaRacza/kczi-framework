#include "uniqdata.hpp"
#include <array>
#include <concepts>
#include <cstddef>
#include <type_traits>
#include <vector>
namespace impl_traits {
template <typename>
struct is_uniqdata : std::false_type {};
template <typename T>
struct is_uniqdata<Uniqdata<T>> : std::true_type {};
template <typename>
struct is_vector : std::false_type {};
template <typename T>
struct is_vector<std::vector<T>> : std::true_type {};
template <typename>
struct is_array : std::false_type {};
template <typename T, size_t Y>
struct is_array<std::array<T, Y>> : std::true_type {};

}; // namespace impl_traits
template <typename T>
concept TrivaiallyCopyable = std::is_trivially_copyable<typename std::remove_cvref<T>::type>::value;
template <typename T>
concept UniqdataType = impl_traits::is_uniqdata<typename std::remove_cvref<T>::type>::value;

template <typename T>
concept VectorType = impl_traits::is_vector<typename std::remove_cvref<T>::type>::value;

template <typename T>
concept ArrayType = impl_traits::is_array<typename std::remove_cvref<T>::type>::value;

template <typename T>
concept Iterable = requires(T obj) {
	obj.begin();
	obj.end();
};
