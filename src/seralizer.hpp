#pragma once

#include <cstddef>
#include <cstring>
#include <span>
#include <stdexcept>
#include <string.h>
#include <tuple>
#include <type_traits>
#include <types.hpp>
#include <types_traits.hpp>
#include <uniqdata.hpp>
namespace impl_seralize {
template <typename T>
concept DynamicSizeCopyable = requires(T type) {
	{type.size()};
	{type.data()};
	{std::is_trivially_copyable<std::remove_pointer<decltype(type.data())>()>()};
	requires Iterable<T>;
	requires !ArrayType<T>;
};
template <typename T>
constexpr inline auto get_size(const T &obj) -> u64 requires TrivaiallyCopyable<T> {
	return sizeof(obj);
}
template <typename T>
inline auto get_size(const T &obj) -> u64 requires UniqdataType<T> {
	return obj.size() * sizeof(*obj.data()) + 4;
}
template <typename T>
inline auto get_size(const T &obj) -> u64 requires VectorType<T> {
	return obj.size() * sizeof(*obj.data()) + 4;
}
template <typename T>
inline auto copy_dst(const u8 *buffer, const T &obj, u64 &offset) noexcept -> void requires TrivaiallyCopyable<T> {
	memcpy((void *)(buffer + offset), (u8 *)&obj, sizeof(obj));
	offset += sizeof(obj);
}
template <typename T>
inline auto copy_dst(const u8 *buffer, const T &obj, u64 &offset) noexcept -> void requires DynamicSizeCopyable<T> {
	copy_dst(buffer, (u32)obj.size(), offset);
	for (const auto &element : obj) {
		copy_dst(buffer, element, offset);
	}
}
template <typename T>
inline auto copy_src(const std::span<const u8> &data, T &obj, u64 &offset) noexcept -> void requires TrivaiallyCopyable<T> {
	memcpy((void *)&obj, (void *)(data.data() + offset), sizeof(obj));
	offset += sizeof(obj);
}
template <typename T>
inline auto copy_src(const std::span<const u8> &data, T &obj, u64 &offset) noexcept -> void requires DynamicSizeCopyable<T> {
	u32 size = 0;
	copy_src(data, size, offset);
	obj.resize(size);
	for (auto &element : obj) {
		copy_src(data, element, offset);
	}
}
template <u32 index, typename T>
inline auto element_size(const T &list) -> u64 {
	u64 curr_size = 0;
	const auto &element = std::get<index>(list);
	curr_size += get_size(element);
	if constexpr (index + 1 < std::tuple_size<T>()) {
		return curr_size + element_size<index + 1>(list);
	}
	return curr_size;
}

inline auto calc_size(const auto &list) -> u64 {
	return element_size<0>(list);
}
template <u32 index, typename T>
inline auto copy_elements_dst(const T &list, const u8 *buffer, u64 &offset) -> void {
	const auto &element = std::get<index>(list);
	copy_dst(buffer, element, offset);
	if constexpr (index + 1 < std::tuple_size<T>()) {
		copy_elements_dst<index + 1>(list, buffer, offset);
	}
}
template <u32 index, typename T>
inline auto copy_elements_src(T &list, const std::span<const u8> &data, u64 &offset) -> void {
	auto &element = std::get<index>(list);
	copy_src(data, element, offset);
	if constexpr (index + 1 < std::tuple_size<T>()) {
		copy_elements_src<index + 1>(list, data, offset);
	}
}
} // namespace impl_seralize
template <typename... Args>
inline auto auto_serialize(const Args &...args) -> Uniqdata<u8> {
	auto list = std::make_tuple(args...);
	u64 seralized = impl_seralize::calc_size(list);
	u64 offset = 0;
	Uniqdata<u8> buffer(seralized);
	impl_seralize::copy_elements_dst<0>(list, buffer.data(), offset);
	return buffer;
}
template <typename... Args, size_t span_extend>
inline auto auto_serialize_in_place(const std::span<u8, span_extend> &span, const Args &...args) -> std::span<u8> {
	auto list = std::make_tuple(args...);
	u64 seralized = impl_seralize::calc_size(list);
	u64 offset = 0;
	impl_seralize::copy_elements_dst<0>(list, span.data(), offset);
	return std::span<u8>(span.data(), span.data() + seralized);
}
template <typename... Args>
inline auto auto_deserialize(const std::span<const u8> &data, Args &&...args) -> u32 {
	auto list = std::tie(args...);
	u64 offset = 0;
	impl_seralize::copy_elements_src<0>(list, data, offset);
	return offset;
}
template <u8 index, typename T>
inline auto get_byte(const T &data) -> u8 {
	const u8 *raw_data = (u8 *)&data;
	return raw_data[index];
}
template <u8 index, typename T>
inline auto set_byte(const T &data, const u8 &value) -> void {
	u8 *raw_data = (u8 *)&data;
	raw_data[index] = value;
}