#pragma once
#include <fstream>
#include <ios>
#include <span>
#include <stdexcept>
#include <string>
#include <string_view>
#include <type_traits>
#include <types.hpp>
#include <utility>
#include <vector>
namespace kczi {
constexpr inline auto pow(const auto &a, const auto &b) -> decltype(a * b) {
	auto sum = decltype(a * b)(1);
	for (u32 i = 0; i < b; i++) {
		sum *= a;
	}
	return sum;
}
auto inline read_file(const std::string &path) -> std::string {
	std::ifstream file(path, std::ios::in);
	if (!file.good()) {
		throw std::runtime_error("cannot opean file: " + path);
	}
	u64 size;
	file.seekg(0, std::ios::end);
	size = file.tellg();
	file.seekg(0, std::ios::beg);
	std::string out;
	out.resize(size);
	file.read(out.data(), size);
	return out;
}
auto inline read_file_to_vec(const std::string &path) -> std::vector<u8> {
	std::ifstream file(path, std::ios::in);
	if (!file.good()) {
		throw std::runtime_error("cannot opean file: " + path);
	}
	u64 size;
	file.seekg(0, std::ios::end);
	size = file.tellg();
	file.seekg(0, std::ios::beg);
	std::vector<u8> out;
	out.resize(size);
	file.read((char *)out.data(), size);
	return out;
}
auto inline write_vec_to_file(const std::string &path, const std::span<u8> &vec) -> void {
	std::ofstream file(path);
	if (!file.good()) {
		throw std::runtime_error("cannot opean file: " + path);
	}
	file.write((const char *)vec.data(), vec.size());
}
template <typename T, typename Y>
class ReadOnly {
	friend Y;

private:
	T value;

public:
	constexpr ReadOnly() = default;
	constexpr ReadOnly(ReadOnly &&) noexcept = default;
	constexpr ReadOnly(ReadOnly &) = default;
	constexpr ~ReadOnly() = default;
	constexpr ReadOnly(T &&t) : value(std::forward(t)) {}
	constexpr auto get() const noexcept -> const T & {
		return value;
	}
	constexpr auto operator->() noexcept -> T & {
		return value;
	}
	constexpr auto operator=(const T &other) {
		value = other;
	}
	constexpr auto operator=(const T &&other) {
		value = std::move(other);
	}
};
} // namespace kczi