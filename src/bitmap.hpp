#pragma once
#include "types.hpp"
#include <algorithm>
#include <array>
#include <iterator>
#include <uniqdata.hpp>
template <typename T, typename Y>
concept BitmapObj = requires(T obj) {
											obj.get_pixel(ty2<u32>(0, 0));
											obj.set_pixel(ty2<u32>(0, 0), 0);
											obj.size();
										};
template <typename T, typename Y>
concept BitmapObjOwning = requires(T obj) {
														{ obj } -> BitmapObj<Y>;
														obj.m_data;
													};
/*
template <typename T, typename Y>
struct BitmapIterator {
	u2_32 m_coord;
	u2_32 m_size;
	T &bitmap;
	BitmapIterator(u2_32 coord, u2_32 size, BitmapObj<Y> auto bitmap) : m_coord(coord), m_size(size) {}
	using refrence = Y &;
	using value_type = Y;
	using pointer = Y *;
};
*/
template <typename T, typename Y>
concept PixelAction = requires(T callback, Y a) {
												callback(a);
											};
template <typename T, typename Y>
concept PixelActionCoord = requires(T callback, Y a) {
														 callback(a, ty2<u32>(0, 0));
													 };

template <typename T = u32>
class Bitmap {
public:
	Uniqdata<T> m_data;
	u2_32 m_size{0, 0};

	Bitmap();
	Bitmap(u2_32 size);
	Bitmap(Bitmap &&b);
	~Bitmap();

	inline auto get_pixel(const u2_32 &coord) const -> T;
	inline auto set_pixel(const u2_32 &coord, const T &val) -> void;
	inline auto for_pixel(PixelAction<T> auto action) -> void;
	inline auto for_pixel(PixelAction<T> auto action) const -> void;
	inline auto for_pixel(PixelActionCoord<T> auto action) -> void;
	inline auto for_pixel(PixelActionCoord<T> auto action) const -> void;

	inline auto resize(const u2_32 &new_size) -> void;
	inline auto size() const noexcept -> u2_32;
	inline auto data() const noexcept -> const T *;
	inline auto operator=(Bitmap &&b) -> Bitmap &;
};
template <typename T>
inline auto Bitmap<T>::size() const noexcept -> u2_32 {
	return m_size;
}
template <typename T>
inline auto Bitmap<T>::get_pixel(const u2_32 &coord) const -> T {
	return m_data[coord.y * m_size.x + coord.x];
}
template <typename T>
inline auto Bitmap<T>::set_pixel(const u2_32 &coord, const T &data) -> void {
	m_data[coord.y * m_size.x + coord.x] = data;
}
template <typename T>
inline auto Bitmap<T>::for_pixel(PixelAction<T> auto action) -> void {
	for (u32 i = 0; i < m_data.size(); i++) {
		action(m_data[i]);
	}
}
template <typename T>
inline auto Bitmap<T>::for_pixel(PixelActionCoord<T> auto action) -> void {
	for (u32 y = 0; y < m_size.y; y++) {
		for (u32 x = 0; x < m_size.x; x++) {
			action(m_data[y * m_size.x + x], ty2(x, y));
		}
	}
}
template <typename T>
inline auto Bitmap<T>::for_pixel(PixelAction<T> auto action) const -> void {
	for (u32 i = 0; i < m_data.size(); i++) {
		action(m_data[i]);
	}
}
template <typename T>
inline auto Bitmap<T>::for_pixel(PixelActionCoord<T> auto action) const -> void {
	for (u32 y = 0; y < m_size.y; y++) {
		for (u32 x = 0; x < m_size.x; x++) {
			action(m_data[y * m_size.x + x], ty2(x, y));
		}
	}
}

template <typename T>
inline auto Bitmap<T>::resize(const u2_32 &new_size) -> void {
	Uniqdata<T> new_m_data(new_size.get_product());
	auto max_y = std::min(m_size.y, new_size.y);
	auto max_x = std::min(m_size.x, new_size.x);
	for (u32 y = 0; y < max_y; y++) {
		for (u32 x = 0; x < max_x; x++) {
			new_m_data[y * new_size.x + x] = m_data[y * m_size.x + x];
		}
	}
}
template <typename T>
inline auto Bitmap<T>::data() const noexcept -> const T * {
	return m_data.data();
}

template <typename T>
Bitmap<T>::Bitmap() {}
template <typename T>
Bitmap<T>::Bitmap(u2_32 size) : m_size(size), m_data(size.get_product()) {}
template <typename T>
Bitmap<T>::Bitmap(Bitmap &&b) : m_size(b.m_size), m_data(std::move(b.m_data)) {
	b.m_size = ty2<u32>(0, 0);
}
template <typename T>
Bitmap<T>::~Bitmap() {}
template <typename T>
inline auto Bitmap<T>::operator=(Bitmap &&b) -> Bitmap & {
	m_data = std::move(b.m_data);
	m_size = b.m_size;
	b.m_size = ty2<u32>(0, 0);
	return *this;
}

namespace kczi_utils {
constexpr u32 dynamic_size = -1;
constexpr u2_32 dynamic_t2 = ty2(dynamic_size, dynamic_size);
} // namespace kczi_utils
template <typename T, u2_32 const_size = kczi_utils::dynamic_t2>
struct BitmapRef {
	T &obj;
	using Y = decltype(obj.get_pixel(ty2<u32>(0, 0)));

	u2_32 m_size = {0, 0};
	u2_32 m_coord = {0, 0};
	BitmapRef(T &obj, u2_32 coord, u2_32 size = {0, 0})
		requires BitmapObjOwning<T, Y>
			: obj(obj), m_size(size), m_coord(coord) {}
	constexpr inline auto size() const -> u2_32 {
		if constexpr (const_size == kczi_utils::dynamic_t2) {
			return m_size;
		}
		return const_size;
	}
	constexpr inline auto get_coord() const -> u2_32 {
		return m_coord;
	}
	constexpr inline auto get_pixel(const u2_32 &coord) const -> Y {
		return obj.get_pixel(coord + m_coord);
	}
	constexpr inline auto set_pixel(const u2_32 &coord, const Y &color) -> void {
		obj.set_pixel(coord + m_coord, color);
	}
	constexpr inline auto for_pixel(PixelAction<Y> auto action) -> void {
		for (u32 y = 0; y < size().y; y++) {
			for (u32 x = 0; x < size().x; x++) {
				action(obj.m_data[(y + m_coord.y) * obj.size().x + (x + m_coord.x)]);
			}
		}
	}
	constexpr inline auto for_pixel(PixelActionCoord<Y> auto action) -> void {
		for (u32 y = 0; y < size().y; y++) {
			for (u32 x = 0; x < size().x; x++) {
				action(obj.m_data[(y + m_coord.y) * obj.size().x + (x + m_coord.x)], ty2<u32>(x, y));
			}
		}
	}
	constexpr inline auto for_pixel(PixelAction<Y> auto action) const -> void {
		for (u32 y = 0; y < size().y; y++) {
			for (u32 x = 0; x < size().x; x++) {
				action(obj.m_data[(y + m_coord.y) * obj.size().x + (x + m_coord.x)]);
			}
		}
	}
	constexpr inline auto for_pixel(PixelActionCoord<Y> auto action) const -> void {
		for (u32 y = 0; y < size().y; y++) {
			for (u32 x = 0; x < size().x; x++) {
				action(obj.m_data[(y + m_coord.y) * obj.size().x + (x + m_coord.x)], ty2<u32>(x, y));
			}
		}
	}
};
template <typename T, u2_32 m_size>
struct StaticMap {
	std::array<T, m_size.get_product()> m_data;
	constexpr inline auto size() -> u2_32 {
		return m_size;
	}
	constexpr inline auto get_pixel(const u2_32 &coord) const -> T {
		return m_data[coord.y * m_size.x + coord.x];
	}
	constexpr inline auto set_pixel(const u2_32 &coord, const T &color) -> void {
		m_data[coord.y * m_size.x + coord.x] = color;
	}
	constexpr inline auto for_pixel(PixelAction<T> auto action) -> void {
		for (u32 y = 0; y < size().y; y++) {
			for (u32 x = 0; x < size().x; x++) {
				action(m_data[x + y * size().x]);
			}
		}
	}
	constexpr inline auto for_pixel(PixelActionCoord<T> auto action) -> void {
		for (u32 y = 0; y < size().y; y++) {
			for (u32 x = 0; x < size().x; x++) {
				action(m_data[x + y * size().x], {x, y});
			}
		}
	}
};
