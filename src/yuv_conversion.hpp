#pragma once
#include "types.hpp"
#include <bitmap.hpp>
#define CLIP(X) ((X) > 255 ? 255 : (X) < 0 ? 0 \
																					 : X)

// YUV -> RGB
#define C(Y) ((Y)-16)
#define D(U) ((U)-128)
#define E(V) ((V)-128)

#define YUV2R(Y, U, V) CLIP((298 * C(Y) + 409 * E(V) + 128) >> 8)
#define YUV2G(Y, U, V) CLIP((298 * C(Y) - 100 * D(U) - 208 * E(V) + 128) >> 8)
#define YUV2B(Y, U, V) CLIP((298 * C(Y) + 516 * D(U) + 128) >> 8)
// RGB -> YUV
#define RGB2Y(R, G, B) CLIP(((66 * (R) + 129 * (G) + 25 * (B) + 128) >> 8) + 16)
#define RGB2U(R, G, B) CLIP(((-38 * (R)-74 * (G) + 112 * (B) + 128) >> 8) + 128)
#define RGB2V(R, G, B) CLIP(((112 * (R)-94 * (G)-18 * (B) + 128) >> 8) + 128)
template <typename T>
inline auto rgb_to_yuv(T &b) -> void requires BitmapObj<T, u32> {
	b.for_pixel([](u32 &px) {
		PixelData data_in(px);
		PixelData data_out{};
		data_out.names.y = RGB2Y(data_in.names.r, data_in.names.g, data_in.names.b);
		data_out.names.u = RGB2U(data_in.names.r, data_in.names.g, data_in.names.b);
		data_out.names.v = RGB2V(data_in.names.r, data_in.names.g, data_in.names.b);
		data_out.names.a = data_in.names.a;
		px = data_out.get();
	});
}
template <typename T>
inline auto yuv_to_rgb(T &b) -> void requires BitmapObj<T, u32> {
	b.for_pixel([](u32 &px) {
		PixelData data_in(px);
		PixelData data_out{};
		data_out.names.r = YUV2R(data_in.names.y, data_in.names.u, data_in.names.v);
		data_out.names.g = YUV2G(data_in.names.y, data_in.names.u, data_in.names.v);
		data_out.names.b = YUV2B(data_in.names.y, data_in.names.u, data_in.names.v);
		data_out.names.a = data_in.names.a;
		px = data_out.get();
	});
}
