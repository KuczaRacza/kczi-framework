#pragma once
#include "types.hpp"
#include <concepts>
#include <optional>
#include <stdexcept>
#include <string_view>
#include <type_traits>
#include <utility>
template <typename T>
concept ErrorType =
		requires(T err) {
			std::is_default_constructible<T>();
			!std::is_trivially_constructible<T>();
			{ err == err } -> std::same_as<bool>;

			err.what();
		};
template <typename T>
concept ValueType =
		requires(T val) {
			std::is_move_assignable<T>();
		};
template <typename T = u8>
class BaseErrType {
public:
	T err{0};
	BaseErrType();
	BaseErrType(T err);
	inline auto operator==(const BaseErrType &other) const -> bool;
	virtual auto what() const -> std::string_view;
};
template <typename T>
BaseErrType<T>::BaseErrType() {}

template <typename T>
BaseErrType<T>::BaseErrType(T err) : err(err) {}

template <typename T>
auto BaseErrType<T>::what() const -> std::string_view {
	if (err == 0) {
		return "ok";
	} else {
		return "Base error";
	}
}
template <typename T>
inline auto BaseErrType<T>::operator==(const BaseErrType &other) const -> bool {
	return err == other.err;
}

template <ValueType ValType, ErrorType ErrType>
class Expected {
public:
	ErrType err{};
	std::optional<ValType> val;
	Expected() = default;
	Expected(const Expected &) = default;
	Expected(Expected &&) = default;
	Expected &operator=(const Expected &) = default;
	Expected &operator=(Expected &&) = default;
	~Expected() = default;
	Expected(ValType &&val) : val(std::move(val)) {}
	Expected(ErrType err) : err(err) {}

	auto get() -> ValType {
		if (err != ErrType{}) [[unlikely]] {
			throw std::runtime_error((std::string)err.what());
		}
		return std::move(val.value());
	}
	[[nodiscard]] auto get(ValType &ref) noexcept -> bool {
		if (err != ErrType{}) [[unlikely]] {
			return false;
		}
		ref = std::move(val.value());
		return true;
	}
	[[nodiscard]] auto get(ValType &ref, ErrType &err_ref) noexcept -> bool {
		if (err != ErrType{}) [[unlikely]] {
			err_ref = err;
			return false;
		}
		ref = std::move(val.value());
	}
	auto ok() const noexcept -> bool {
		return err == ErrType{};
	}
	auto clear() noexcept -> ErrType {
		auto tmp = std::move(err);
		err = ErrType{};
		return tmp;
	}
};