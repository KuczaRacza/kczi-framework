#pragma once
#include <algorithm>
#include <cstddef>
#include <initializer_list>
#include <iterator>
#include <new>
#include <span>
#include <stdexcept>
#include <tuple>
#include <type_traits>
#include <types.hpp>
#include <vector>
template <typename T>
class Uniqdata {
private:
	u64 len = 0;
	T *ptr = nullptr;

public:
	inline auto size() const noexcept -> u64;

	struct Iterator {
		using iterator_category = std::contiguous_iterator_tag;
		using difference_type = std::ptrdiff_t;
		using value_type = T;
		using pointer = T *;
		using reference = T &;
		pointer m_ptr = nullptr;
		Iterator(pointer ptr) : m_ptr(ptr) {}
		Iterator() {}
		reference operator*() const { return *m_ptr; }
		pointer operator->() const { return m_ptr; }
		Iterator &operator++() {
			m_ptr++;
			return *this;
		}
		Iterator operator++(int) {
			Iterator tmp = *this;
			++(*this);
			return tmp;
		}
		Iterator &operator--() {
			m_ptr--;
			return *this;
		}
		Iterator operator--(int) {
			Iterator tmp = *this;
			--(*this);
			return tmp;
		}
		friend long operator-(const Iterator &a, const Iterator &b) {
			return (size_t)a.m_ptr - (size_t)b.m_ptr;
		}
		friend auto operator-(const Iterator &a, const difference_type &b) {
			return Iterator(a.m_ptr - b);
		}
		friend auto operator+(const Iterator &a, const difference_type &b) -> Iterator {
			return Iterator(a.m_ptr + b);
		}
		friend auto operator+(const difference_type &b, const Iterator &a) -> Iterator {
			return Iterator(a.m_ptr + b);
		}
		friend auto operator+=(Iterator &a, const auto &b) -> Iterator & {
			a.m_ptr += b.m_ptr;
			return a;
		}
		friend auto operator-=(Iterator &a, const auto &b) -> Iterator & {
			a.m_ptr -= b.m_ptr;
			return a;
		}
		friend bool operator==(const Iterator &a, const Iterator &b) {
			return a.m_ptr == b.m_ptr;
		};
		friend bool operator>(const Iterator &a, const Iterator &b) {
			return a.m_ptr > b.m_ptr;
		};
		friend bool operator<(const Iterator &a, const Iterator &b) {
			return a.m_ptr < b.m_ptr;
		};
		friend bool operator>=(const Iterator &a, const Iterator &b) {
			return a.m_ptr >= b.m_ptr;
		};
		friend bool operator<=(const Iterator &a, const Iterator &b) {
			return a.m_ptr <= b.m_ptr;
		};
		friend bool operator!=(const Iterator &a, const Iterator &b) {
			return a.m_ptr != b.m_ptr;
		};
		auto operator[](difference_type index) const ->  T & {
			return *(m_ptr + index);
		}
	};
	Iterator begin() const { return Iterator(ptr); }
	Iterator end() const { return Iterator(ptr + size()); }
	Uniqdata(Uniqdata &&u);
	Uniqdata(const Uniqdata &u);

	inline auto operator=(Uniqdata &&u) -> Uniqdata &;
	inline auto resize(u64 len) -> void;
	inline auto operator=(const Uniqdata &other) -> Uniqdata &;
	inline auto operator=(const std::vector<T> &other) -> Uniqdata &;
	inline auto operator[](const u64 &i) -> T &;
	inline auto operator[](const u64 &i) const -> const T &;

	Uniqdata();
	Uniqdata(const std::initializer_list<T> &other);
	Uniqdata(u32 len);
	Uniqdata(const std::vector<T> &v);
	~Uniqdata();

	template <typename U, size_t Y>
	inline auto push(const std::span<U, Y> &data) -> Uniqdata &;
	inline auto as_span() const noexcept -> std::span<T>;
	inline auto as_bytes() const noexcept -> std::span<u8>;
	inline auto as_vec() const -> std::vector<T>;
	inline auto as_tuple() const -> std::tuple<T *, u64>;
	inline auto data() const -> const T *;
};

template <typename T>
Uniqdata<T>::Uniqdata() {}
template <typename T>
Uniqdata<T>::~Uniqdata() { delete[] ptr; }
template <typename T>
Uniqdata<T>::Uniqdata(u32 len) {
	ptr = new T[len]();
	this->len = len;
};
template <typename T>
inline auto Uniqdata<T>::size() const noexcept -> u64 {
	return len;
}
template <typename T>
Uniqdata<T>::Uniqdata(const std::initializer_list<T> &other) {
	ptr = new T[other.size()];
	len = other.size();
	std::copy(other.begin(), other.end(), begin());
}
template <typename T>
Uniqdata<T>::Uniqdata(Uniqdata &&u) : len(u.len), ptr(u.ptr) {
	u.len = 0;
	u.ptr = nullptr;
};
template <typename T>
inline auto Uniqdata<T>::operator=(Uniqdata &&u) -> Uniqdata & {
	delete[] ptr;
	len = u.len;
	ptr = u.ptr;
	u.len = 0;
	u.ptr = nullptr;
	return *this;
}

template <typename T>
inline auto Uniqdata<T>::resize(u64 len) -> void {
	if (len == this->len) {
		return;
	}
	Uniqdata<T> tmp(len);
	std::copy(begin(), begin() + len, tmp.begin());
	delete[] ptr;
	ptr = tmp.ptr;
	tmp.ptr = nullptr;
	this->len = len;
}
template <typename T>
Uniqdata<T>::Uniqdata(const std::vector<T> &v) {
	ptr = new T[v.size()];
	len = v.size();
	std::copy(v.begin(), v.end(), begin());
}
template <typename T>
inline auto Uniqdata<T>::operator=(const std::vector<T> &other) -> Uniqdata & {
	resize(other.size());
	std::copy(other.begin(), other.end(), begin());
}
template <typename T>
inline auto Uniqdata<T>::operator=(const Uniqdata &other) -> Uniqdata & {
	resize(other.size());
	std::copy(other.begin(), other.end(), begin());
	return *this;
}
template <typename T>
inline auto Uniqdata<T>::operator[](const u64 &i) -> T & {
#ifndef NDEBUG
	if (i >= size()) {
		throw std::runtime_error("Out of bounds");
	}
#endif
	return ptr[i];
}
template <typename T>
inline auto Uniqdata<T>::operator[](const u64 &i) const -> const T & {
#ifndef NDEBUG
	if (i >= size()) {
		throw std::runtime_error("Out of bounds");
	}
#endif
	return ptr[i];
}
template <typename T>
template <typename U, size_t Y>
inline auto Uniqdata<T>::push(const std::span<U, Y> &other) -> Uniqdata & {
	static_assert(std::is_convertible<U, T>(), "Cannot convert types");
	u64 old_len = size();
	if constexpr (Y == std::dynamic_extent) {
		resize(len + other.size());
		std::copy(other.begin(), other.end(), begin() + old_len);
	} else {
		resize(len + Y);
		for (u32 i = 0; i < Y; i++) {
			ptr[i + old_len] = other[i];
		}
	}
	return *this;
}
template <typename T>
inline auto Uniqdata<T>::as_span() const noexcept -> std::span<T> {
	return std::span<T>(this->ptr, size());
}
template <typename T>
inline auto Uniqdata<T>::as_bytes() const noexcept -> std::span<u8> {
	return std::span<u8>((u8 *)this->ptr, size() * sizeof(T));
}
template <typename T>
inline auto Uniqdata<T>::as_vec() const -> std::vector<T> {
	std::vector<T> vec(size());
	std::copy(begin(), end(), vec.begin());
	return vec;
}
template <typename T>
inline auto Uniqdata<T>::data() const -> const T * {
	return ptr;
}
template <typename T>
inline auto Uniqdata<T>::as_tuple() const -> std::tuple<T *, u64> {
	return std::make_tuple(ptr, len);
}
template <typename T>
Uniqdata<T>::Uniqdata(const Uniqdata &u) {
	*this = u;
}