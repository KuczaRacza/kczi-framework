#pragma once
#include <array>
#include <cstdint>
#include <cstring>
#include <unordered_map>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;
typedef float f32;
typedef double f64;

constexpr i8 max_i8 = INT8_MAX;
constexpr i16 max_i16 = INT16_MAX;
constexpr i32 max_i32 = INT32_MAX;
constexpr i64 max_i64 = INT64_MAX;
constexpr u8 max_u8 = UINT8_MAX;
constexpr u16 max_u16 = UINT16_MAX;
constexpr u32 max_u32 = UINT32_MAX;
constexpr u64 max_u64 = UINT64_MAX;
struct u24 {
	std::array<u8, 3> data;
	constexpr u24(const u32 &px) {
		data[0] = px & 0xff;
		data[1] = (px & 0x00ff) << 8;
		data[2] = (px & 0x0000ff) << 16;
	}
	constexpr u24() {
		data.fill(0);
	}
	constexpr auto as_u32() const -> u32 {
		return data[0] | data[1] << 8 | data[2] << 16 | 0;
	}
};
struct PixelData {
	struct ColorNames {
		union {
			u8 r;
			u8 y;
		};
		union {
			u8 g;
			u8 u;
		};
		union {
			u8 b;
			u8 v;
		};
		u8 a;
	};
	union {
		ColorNames names;
		std::array<u8, 4> data;
	};

	constexpr auto get() const -> u32 {
		return names.r | names.g << 8 | names.b << 16 | names.a << 24;
	}
	constexpr auto operator=(const u32 &val) -> PixelData & {
		names.a = val & 0xff;
		names.b = (val >> 8) & 0xff;
		names.g = (val >> 16) & 0xff;
		names.r = (val >> 24) & 0xff;
		return *this;
	}
	auto operator=(const u24 &val) -> PixelData & {
		names.r = val.data[0];
		names.g = val.data[1];
		names.b = val.data[2];
		names.a = 0;
		return *this;
	}
	PixelData() {
		names.r = 0;
		names.g = 0;
		names.b = 0;
		names.a = 0;
	}
	PixelData(const u32 &data) { *this = data; }
	PixelData(const u24 &data) { *this = data; }
	template <u8 depth = 4>
	inline auto for_channel(auto action) -> void {
		for (u32 i = 0; i < depth; i++) {
			action(data[i]);
		}
	}
};
template <typename T>
struct Type2 {
	T x;
	T y;
	constexpr inline Type2<T> operator+(const Type2<T> &t1) const noexcept {
		Type2<T> tmp = {x + t1.x, y + t1.y};
		return tmp;
	};
	constexpr inline Type2<T> operator-(const Type2<T> &t1) const noexcept {
		Type2<T> tmp = {x - t1.x, y - t1.y};
		return tmp;
	};
	constexpr inline Type2<T> operator*(const Type2<T> &t1) const noexcept {
		Type2<T> tmp = {x * t1.x, y * t1.y};
		return tmp;
	};
	constexpr inline Type2<T> operator/(const Type2<T> &t1) const noexcept {
		Type2<T> tmp = {x / t1.x, y / t1.y};
		return tmp;
	};
	constexpr inline Type2<T> operator%(const Type2<T> &t1) const noexcept {
		Type2<T> tmp = {x % t1.x, y % t1.y};
		return tmp;
	};
	constexpr inline T get_product() const noexcept { return x * y; }
	constexpr inline T get_ratio() const noexcept { return x / y; }
	constexpr inline T get_reverse_ratio() const noexcept { return y / x; }
	constexpr inline bool operator==(const Type2<T> &t) const noexcept {
		return (t.x == x) && (t.y == y);
	}
	constexpr Type2<T> half() const {
		return {x / (T)2, y / (T)2};
	}
};
template <typename T, typename Y>
inline Type2<T> type2_cast(Type2<Y> t2) noexcept {
	Type2<T> ret = {(T)(t2.x), (T)(t2.y)};
	return ret;
}
template <typename T>
constexpr bool is_coliding(Type2<T> point, Type2<T> pos, Type2<T> end) {
	if (point.x > pos.x && point.x < end.x && point.y > pos.y &&
			point.y < end.y) {
		return true;
	}
	return false;
}
template <typename T>
constexpr inline auto ty2(const T &x, const T &y) {
	return Type2<T>({x, y});
}
typedef Type2<int32_t> i2_32;
typedef Type2<int64_t> i2_64;
typedef Type2<int16_t> i2_16;
typedef Type2<uint64_t> u2_64;
typedef Type2<uint16_t> u2_16;
typedef Type2<uint32_t> u2_32;
typedef Type2<f32> f2_32;
typedef Type2<double> f2_64;
template <typename T>
struct std::hash<Type2<T>> {
	std::size_t operator()(Type2<T> const &s) const noexcept {
		std::size_t h1 = std::hash<T>{}(s.x);
		std::size_t h2 = std::hash<T>{}(s.y);
		return h1 ^ (h2 << 1);
	}
};
namespace kczi {

template <typename T>
class rect {
public:
	T x;
	T y;
	T w;
	T h;
	constexpr rect() = default;
	constexpr rect(const rect &) = default;
	constexpr ~rect() = default;
	constexpr rect(const T &x, const T &y, const T &w, const T &h) : x(x), y(y), w(w), h(h) {}
	constexpr rect(const Type2<T> &coord, const Type2<T> &size) : x(coord.x), y(coord.y), w(size.x), h(size.y) {}
	constexpr auto end_coord() const -> Type2<T> { return ty2<T>(x + w, y + h); }
	constexpr auto coord() const -> Type2<T> { return ty2<T>(x, y); }
	constexpr auto size() const -> Type2<T> { return ty2<T>(w, h); }
	constexpr auto area() const -> T { return size().get_product(); }
};
using u4_64 = rect<u64>;
using u4_32 = rect<u32>;
using u4_16 = rect<u16>;
using u4_8 = rect<u8>;
using i4_64 = rect<i64>;
using i4_32 = rect<i32>;
using i4_16 = rect<i16>;
using i4_8 = rect<i8>;
using f4_32 = rect<f32>;
using f4_64 = rect<f2_64>;

} // namespace kczi
template <typename T>
struct std::hash<kczi::rect<T>> {
	std::size_t operator()(kczi::rect<T> const &s) const noexcept {
		std::size_t h1 = std::hash<T>{}(s.x);
		std::size_t h2 = std::hash<T>{}(s.y);
		std::size_t h3 = std::hash<T>{}(s.w);
		std::size_t h4 = std::hash<T>{}(s.h);
		return h1 ^ (h2 << 1) ^ (h3 >> 1) ^ (h4 << 2);
	}
};